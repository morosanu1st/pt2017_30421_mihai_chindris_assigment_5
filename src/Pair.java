import java.io.Serializable;

public class Pair<Left,Right> implements Serializable {
	public Left l;
	public Right r;
	public Pair(Left l, Right r) {
		this.l = l;
		this.r = r;
	}
}
