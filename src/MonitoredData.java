import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData {
	 LocalDateTime startTime;
	 LocalDateTime endTime;
	 String activityLabel;
	 public  MonitoredData(String st,String et,String activ){
		 DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		 startTime=LocalDateTime.parse(st, formatter);
		 endTime=LocalDateTime.parse(et, formatter);
		 activityLabel=activ;
	 }
	 public static String getActivityLabel(MonitoredData x){
	 	return x.activityLabel;}
	public LocalDateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	public LocalDateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
//	public String getActivityLabel() {
//		return activityLabel;
//	}
	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	@Override
	public String toString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		return "MonitoredData [startTime=" + startTime.format(formatter) + ", endTime=" + endTime.format(formatter) + ", activityLabel=" + activityLabel
				+ "]";
	}
}
